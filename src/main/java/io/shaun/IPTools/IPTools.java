package io.shaun.IPTools;

import io.shaun.IPTools.locator.LocatedAddress;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.net.*;

public class IPTools {

    static ObjectMapper objectMapper = new ObjectMapper();

    // Locate, using ip-api.com, an IP address, then map it to the POJO.
    public static LocatedAddress locateAddress(String address) {
        try {
            URL ipDetails = new URL("http://ip-api.com/json/" + address);
            return objectMapper.readValue(ipDetails, LocatedAddress.class);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static LocatedAddress locateAddress(InetAddress address){
        return locateAddress(address.toString());
    }

    public static String getHostnameFromIp(String ip){
        try {
            return InetAddress.getByName(ip).getCanonicalHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getHostnameFromIp(InetAddress inetAddress){
        return getHostnameFromIp(inetAddress.toString());
    }

}

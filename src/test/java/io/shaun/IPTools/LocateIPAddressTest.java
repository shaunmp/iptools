package io.shaun.IPTools;

import io.shaun.IPTools.locator.LocatedAddress;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.Assert.assertEquals;

public class LocateIPAddressTest {

    @Test
    public void checkCityStateZipMatchString(){
        LocatedAddress locatedAddress = IPTools.locateAddress("129.78.56.202");
        assertEquals(locatedAddress.getCity(), "Sydney");
        assertEquals(locatedAddress.getRegion(), "NSW");
    }

    @Test
    public void checkCityStateZipMatchINetAddress(){
        LocatedAddress locatedAddress = null;
        try {
            locatedAddress = IPTools.locateAddress(InetAddress.getByName("129.78.56.202"));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        assertEquals(locatedAddress.getCity(), "Sydney");
        assertEquals(locatedAddress.getRegion(), "NSW");
    }

}

package io.shaun.IPTools;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IPToHostnameTest {

    @Test
    public void testOnGoogleIP(){
        assertEquals("google-public-dns-a.google.com", IPTools.getHostnameFromIp("8.8.8.8"));
    }
}
